<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package eshop
 */

?>

</div>

<footer class="footer">
	<div class="content has-text-centered">
		<p class="has-text-primary is-size-1">
			<i class="fab fa-amazon-pay"></i>
			<i class="fab fa-apple-pay"></i>
			<i class="fab fa-cc-mastercard"></i>
			<i class="fab fa-cc-visa"></i>
			<i class="fab fa-cc-paypal"></i>
		</p>
		<p>
			<strong>Bulma</strong> by <a href="https://jgthms.com">Jeremy Thomas</a>. The source code is licensed
			<a href="http://opensource.org/licenses/mit-license.php">MIT</a>. The website content
			is licensed <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY NC SA 4.0</a>.
		</p>
	</div>
</footer>

<?php wp_footer(); ?>

</body>
</html>
