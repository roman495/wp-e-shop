<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package eshop
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>

<div class="container">

<!-- Main container -->
<nav class="level has-text-primary mt-3">
	<div class="level-left">
		<p class="level-item">
			<a href="#login" class="has-text-primary">
				<i class="fas fa-sign-in-alt"></i>
				Log in
			</a>
		</p>
		<p class="level-item">
			<a href="#signup" class="has-text-primary">
				<i class="fas fa-user-plus"></i>
				Sign Up
			</a>
		</p>
	</div>

	<div class="level-right">
		<p class="level-item">
			<i class="fas fa-ruble-sign mr-1"></i>	
			0.00 ( 0 )
		</p>
		<p class="level-item">
			<a href="#cart" class="has-text-primary">
				<i class="fas fa-shopping-cart"></i>
				Cart
			</a>
		</p>
	</div>
</nav>

<nav class="navbar" role="navigation" aria-label="main navigation">
  <div class="navbar-brand">
    <a class="navbar-item" href="<?php home_url() ?>">
		<i class="fas fa-store-alt fa-2x"></i>
		<span class="is-size-4 is-uppercase ml-2"><?php bloginfo( 'name' ) ?></span>
    </a>

    <a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
    </a>
  </div>

  <div id="navbarBasicExample" class="navbar-menu">
    <div class="navbar-start">
      <a class="navbar-item">
        Home
      </a>

      <a class="navbar-item">
        Documentation
      </a>

      <div class="navbar-item has-dropdown is-hoverable">
        <a class="navbar-link">
          More
        </a>

        <div class="navbar-dropdown">
          <a class="navbar-item">
            About
          </a>
          <a class="navbar-item">
            Jobs
          </a>
          <a class="navbar-item">
            Contact
          </a>
          <hr class="navbar-divider">
          <a class="navbar-item">
            Report an issue
          </a>
        </div>
      </div>
    </div>

    <div class="navbar-end">
      <div class="navbar-item">
        <div class="buttons">
          <a class="button is-primary">
            <strong>Sign up</strong>
          </a>
          <a class="button is-light">
            Log in
          </a>
        </div>
      </div>
    </div>
  </div>
</nav>