<?php
get_header();
?>

<section class="hero is-medium is-link">
  <div class="hero-body">
    <p class="title">
      Info hero
    </p>
    <p class="subtitle">
      Info subtitle
    </p>
    <a 
        href="<?php echo get_option( 'url_slide' ) ?>" 
        class="button is-link is-inverted are-large is-uppercase"
    >
        <?php echo get_option( 'button_slide' ) ?>
    </a>
  </div>
</section>

<section class="section">
    <div class="columns has-text-centered">
        <div class="column is-one-third">
            <div class="box">
                <span class="title is-4 mr-3">Follow us : </span>
                <i class="fab fa-facebook fa-3x mr-3"></i>
                <i class="fab fa-twitter fa-3x"></i>
            </div>
        </div>
        <div class="column is-one-third">
            <div class="box">
                <div class="media">
                    <div class="media-left">
                        <i class="fas fa-truck fa-3x"></i>
                    </div>
                    <div class="media-content">
                        <p class="title is-4">Free shipping</p>
                        <p class="subtitle is-6">on orders over $ 199</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="column is-one-third">
            <div class="box">
                <p class="subtitle is-6">Order online</p>
                <p class="title is-4">Tel: <?php echo get_option( 'my_phone' ) ?></p>
            </div>
        </div>
    </div>
</section>

<?php
get_footer();