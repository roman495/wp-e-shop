<?php
/**
 * eshop functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package eshop
 */

function dd( $data )
{
    $result = <<<TEXT
    <div id='php-dump'>
        <style type="text/css" scoped>
            pre {
                font-size: 16px;
                font-family: 'JetBrains Mono';
                background: #34495e;
                color: #2ecc71;
                margin: 15px;
                padding: 15px;
				border-radius: 5px;
            }
        </style>
TEXT;

    $result .= '<pre>';
    $result .= print_r($data, 1);
    $result .= '</pre>';

    echo $result . '</div>';
}

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'eshop_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function eshop_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on eshop, use a find and replace
		 * to change 'eshop' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'eshop', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__( 'Primary', 'eshop' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'eshop_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);

		add_theme_support( 'woocommerce' );
	}
endif;
add_action( 'after_setup_theme', 'eshop_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function eshop_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'eshop_content_width', 640 );
}
add_action( 'after_setup_theme', 'eshop_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function eshop_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'eshop' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'eshop' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'eshop_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function eshop_scripts() {
	wp_enqueue_style( 'eshop-style', get_stylesheet_uri(), array(), _S_VERSION );
	wp_enqueue_style( 'eshop-bulma', 'https://cdn.jsdelivr.net/npm/bulma@0.9.2/css/bulma.min.css' );
	wp_enqueue_style( 'eshop-fa', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css' );
	wp_style_add_data( 'eshop-style', 'rtl', 'replace' );

	wp_enqueue_script( 'eshop-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'eshop_scripts' );

add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );

function eshop_options() {

	add_settings_field( 
		'url_slide', 
		__( 'Ссылка слайдера', 'eshop' ),
		'display_url',
		'general'
	);

	register_setting( 'general', 'url_slide' );

	add_settings_field( 
		'button_slide', 
		__( 'Заголовок кнопки слайдера', 'eshop' ),
		'display_button',
		'general'
	);

	register_setting( 'general', 'button_slide' );

	add_settings_field( 
		'my_phone', 
		__( 'Мой телефон', 'eshop' ),
		'display_phone',
		'discussion'
	);

	register_setting( 'discussion', 'my_phone' );
}

function display_url() {
	echo '<input type="text" class="regular-text" name="url_slide" value="'.esc_attr( get_option( 'url_slide' ) ).'">';
}

function display_button() {
	echo '<input type="text" class="regular-text" name="button_slide" value="'.esc_attr( get_option( 'button_slide' ) ).'">';
}

function display_phone() {
	echo '<input type="text" class="regular-text" name="my_phone" value="'.esc_attr( get_option( 'my_phone' ) ).'">';
}

add_action( 'admin_menu', 'eshop_options' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

